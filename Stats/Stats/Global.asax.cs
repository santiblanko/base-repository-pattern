﻿using System.Web;
using System.Web.Http;
using Stats.App_Start;
using Autofac;
using Autofac.Integration.WebApi;
using System.Reflection;
using Stats.Models;
using Stats.DataAccess;

namespace Stats {
    public class WebApiApplication : HttpApplication {
        protected void Application_Start( ) {


            GlobalConfiguration.Configure( WebApiConfig.Register );

            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<ModelFactory>().As<IModelFactory>();

            builder.RegisterType<StatsService>().As<IStatsService>();

            var container = builder.Build();

            var resolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configuration.DependencyResolver = resolver;

        }
    }
}