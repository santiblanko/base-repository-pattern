Repository Pattern using ASP.net webAPI
--
1. Crear proyecto.
--
2. Configurar proyecto.
--
3. Instalar entity framework codefirst
--
4. Agregar entidades y poblar datos iniciales.
--
5. Crear repositorios.
--
6. Crear capa de servicio de repositorios.
--
7. Crear controladores, acciones y modelos.
--
8. Validar datos.
--
9. Agregar controlador base.
--
10. Agregar IOC Container (Autofac)
--